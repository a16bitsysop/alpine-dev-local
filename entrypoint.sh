#!/bin/sh
: "${NME:="devuser"}"
: "${ASHELL:="sh"}"

die() {
  echo "$@"
  exit 1
}

[ ! -f ~/.gitconfig ] && die "~/.gitconfig missing"
NAME="$(git config user.name)"
EMAIL="$(git config user.email)"
echo "Using: git name: $NAME - git email: $EMAIL - shell: $ASHELL"
echo
[ ! -d ~/.abuild ] && abuild-keygen -a -i -n

doas sed "s/^#PACKAGER=.*/PACKAGER=\"$NAME \<$EMAIL>\"/" -i /etc/abuild.conf
doas sed "s/^#MAINTAINER/MAINTAINER/" -i /etc/abuild.conf

doas apk update && doas apk upgrade

/usr/bin/env "$ASHELL"
