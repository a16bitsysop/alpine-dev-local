ARG DVER=edge
ARG HOME=/home/${NME}

FROM docker.io/alpine:${DVER}
ENV NME=devuser
ENV ASHELL=sh
ARG HOME
LABEL maintainer "Duncan Bellamy <dunk@denkimushi.com>"

RUN apk update \
&& apk upgrade --available \
&& apk add --no-cache -u \
    alpine-sdk \
    atools \
    ccache \
    doas-sudo-shim \
    glab \
    gnupg \
    nano \
    openssh-client \
    pax-utils \
&& apk add --no-cache -u bash \
&& adduser -D ${NME} && addgroup ${NME} abuild \
&& mkdir /home/${NME}/packages && chown ${NME}:${NME} /home/${NME}/packages \
&& cd /home/${NME} && mkdir aports working && chown ${NME}:${NME} aports working

RUN echo "permit nopass ${NME}" >> /etc/doas.d/doas.conf

# don't remove deps after failed build, enable colours and ccache
RUN sed -e "s/.*ERROR_CLEANUP.*/ERROR_CLEANUP=\"bldroot\"/" -e "s/#USE/USE/g" -i /etc/abuild.conf

RUN if echo $DVER | grep -q "edge"; then echo 'https://dl-cdn.alpinelinux.org/alpine/edge/testing' >> /etc/apk/repositories; fi

WORKDIR /usr/local/bin
COPY --chmod=755 entrypoint.sh build-aport.sh ./
COPY --chmod=755 bump-deps ./bump-deps

WORKDIR /home/${NME}
COPY --chown=${NME} profile /home/${NME}/.profile

USER ${NME}
CMD [ "entrypoint.sh" ]
