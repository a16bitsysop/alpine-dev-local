# alpine-dev-local

Docker container to work on alpine aports

## Getting started

The `alpine-dev` script expects your alpine aports git repo to be in `~/aports` or linked to it.

To use alpine-dev-local copy the script `alpine-dev` to `/usr/local/bin`,
when run this mounts any of the following things into the container if they exist:

* `~/.gitignore`
* `~/.gitconfig`
* `~/.gitlab`
* `~/.gnupg`
* `~/.ssh`
* `~/aports`
* `~/.abuild`

`~/.gitconfig` is not optional as the user name and email is used from it for the
`PACKAGER` and `MAINTAINER` setting in `abuild.conf`.  These need to be present for
merge requests also.

Also the current working directory is mounted into ~/working

## ssh

The .ssh directory is mounted into the running container if present, to use the
key in the container for pushing to alpine gitlab the following entry is required in ~/.ssh/config:
```
Host gitlab.alpinelinux.org
  IdentityFile ~/.ssh/MY_PRIVATE_KEY
```

## git

To use the alpine pre merge check in the `[core]` section of `~/.aports/.git/config`
add `hooksPath = .githooks`

## ccache

By default using ccache is enabled in /etc/abuild.conf

### Useful aliases

These can be added globaly to `~/gitconfig` or just for aports to `~/aports/.git/config`.

To update aports with `git up` (or any other git) set an upstream repo from the https url.
To clean the src directories from aports add the alp-clean alias to `~/.gitconfig`:
```
[alias]
        up = !git fetch && git fetch upstream && git checkout master && git merge upstream/master && git push origin master
        alp-clean = !find $HOME/aports -type d '(' -name "src" -o -name "pkg" -o -name "a" -o -name "b" ')' -print -delete
```

Then a repository can be updated to upstream using the `git up` command

## shell

The `ASHELL` environment variable sets the shell used at runtime, current busybox sh and standalone bash are installed.
