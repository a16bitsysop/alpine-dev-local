#!/bin/sh

die() {
  echo "$1"
  exit 1
}

[ -d "$HOME"/.abuild ] && doas chown -R $(whoami):$(whoami) "$HOME"/.abuild
if [ -f "$HOME"/.abuild/abuild.conf ]; then
  echo "Using existing $HOME/.abuild/abuild.conf"
else
  abuild-keygen -a -i -n
fi
doas apk update
doas apk upgrade --available
echo "Arch is: $(uname -m)"

[ ! -d /deploy ] && die "/deploy is not mounted for the output"
cd /aport || die "aport directory not mounted into /aport"

abuild checksum
abuild -c -r -P /tmp/pkg || die "build failed"
source APKBUILD

doas mkdir -p /deploy/"$pkgname"
doas cp -a /tmp/pkg/* /deploy/"$pkgname"/
doas mkdir -p /deploy/keys
doas cp -a "$HOME"/.abuild/*.pub /deploy/keys/
ls -lahR /deploy
