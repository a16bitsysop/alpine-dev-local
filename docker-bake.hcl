variable "BRANCH" {
  default = "edge"
}

variable "CI_PIPELINE_CREATED_AT" {
  default = "now"
}

variable "IMAGE_TAG" {
  default = "testing"
}

variable "CI_COMMIT_SHA" {
  default = ""
}

variable "CI_PROJECT_DIR" {
  default = ""
}

variable "CI_PROJECT_PATH" {
  default = ""
}

variable "CI_REGISTRY" {
  default = ""
}

variable "DOCKER_REPO" {
  default = ""
}

variable "DOCKER_UN" {
  default = "UNSET"
}

variable "DOCKER_PW" {
  default = ""
}

variable "DESC" {
  default = "docker image"
}

target "image" {

  labels = {
    "org.label-schema.schema-version" = "1.0",
    "org.label-schema.build-date" = "${CI_PIPELINE_CREATED_AT}",
    "org.label-schema.vcs-ref" = "${CI_COMMIT_SHA}",
    "org.label-schema.vcs-url" = "https://gitlab.com/${CI_PROJECT_PATH}",
    "org.label-schema.name" = "${DOCKER_REPO}",
    "org.label-schema.description" = "${DESC}",
    "alpine-version" = "${BRANCH}",
  }
  args = {
    "DVER" = "${BRANCH}"
  }
  tags = [
    "${CI_COMMIT_SHA}"
  ]
}

target "test-image" {
  inherits = [ "image" ]
  platforms = [
    "linux/amd64",
    "linux/arm64"
  ]
  tags = [
    "${CI_REGISTRY}/${CI_PROJECT_PATH}:${CI_COMMIT_SHA}"
  ]
}

target "multi-arch" {
  inherits = [ "image" ]
  platforms = [
    "linux/amd64",
    "linux/386",
    "linux/aarch64",
    "linux/arm/v7",
    "linux/arm/v6",
    "linux/ppc64le",
    "linux/s390x",
    notequal("3.19",BRANCH) ? "linux/riscv64": ""
  ]
}

target "docker-hub" {
  inherits = [ "multi-arch" ]
  args = {
    "DVER" = "${BRANCH}"
  }
  tags = [
    equal("edge",BRANCH) ? "docker.io/${DOCKER_UN}/${DOCKER_REPO}": "docker.io/${DOCKER_UN}/${DOCKER_REPO}:${BRANCH}",
    equal("edge",BRANCH) ? "docker.io/${DOCKER_UN}/${DOCKER_REPO}:${CI_COMMIT_SHA}": ""
  ]
}

target "gitlab" {
  inherits = [ "multi-arch" ]
  args = {
    "DVER" = "${BRANCH}"
  }
  tags = [
    "${CI_REGISTRY}/${CI_PROJECT_PATH}/main:${BRANCH}",
    equal("edge",BRANCH) ? "${CI_REGISTRY}/${CI_PROJECT_PATH}/main": "",
    equal("edge",BRANCH) ? "${CI_REGISTRY}/${CI_PROJECT_PATH}/main:${CI_COMMIT_SHA}": ""

  ]
}
